Source: libdbd-xbase-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>,
           Damyan Ivanov <dmn@debian.org>,
           Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl,
                     libdbi-perl
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libdbd-xbase-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libdbd-xbase-perl.git
Homepage: https://metacpan.org/release/DBD-XBase

Package: libdbd-xbase-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libdbi-perl
Description: Perl module to access xbase files (optionally through DBI)
 DBD::XBase allows creation, access and modification of .dbf (dBase,
 Clipper, Fox* style) database files. It is capable of handling memo
 files, but indexes only in a somewhat limited way. It has two
 interfaces: one using SQL commands (through DBI) ad the other with a
 simple OO interface.
 .
 You can use the included dbf_dump program to dump the content of a .dbf file.
